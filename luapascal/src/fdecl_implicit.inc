  (*
  ** state manipulation
  *)
  function lua_newstate(f: lua_Alloc; ud: Pointer): lua_State; cdecl; external cLuaLibFileName;
  procedure lua_close(L: lua_State); cdecl; external cLuaLibFileName;
  function lua_newthread(L: lua_State): lua_State; cdecl; external cLuaLibFileName;
  function lua_atpanic(L: lua_State; panicf: lua_CFunction): lua_CFunction; cdecl; external cLuaLibFileName;

  (*
  ** basic stack manipulation
  *)
  function lua_gettop(L: lua_State): Integer; cdecl; external cLuaLibFileName;
  procedure lua_settop(L: lua_State; idx: Integer); cdecl; external cLuaLibFileName;
  procedure lua_pushvalue(L: lua_State; idx: Integer); cdecl; external cLuaLibFileName;
  procedure lua_remove(L: lua_State; idx: Integer); cdecl; external cLuaLibFileName;
  procedure lua_insert(L: lua_State; idx: Integer); cdecl; external cLuaLibFileName;
  procedure lua_replace(L: lua_State; idx: Integer); cdecl; external cLuaLibFileName;
  function lua_checkstack(L: lua_State; extra: Integer): LongBool; cdecl; external cLuaLibFileName;
  procedure lua_xmove(from, dest: lua_State; n: Integer); cdecl; external cLuaLibFileName;

  (*
  ** access functions (stack -> C/Pascal)
  *)
  function lua_isnumber(L: lua_State; idx: Integer): LongBool; cdecl; external cLuaLibFileName;
  function lua_isstring(L: lua_State; idx: Integer): LongBool; cdecl; external cLuaLibFileName;
  function lua_iscfunction(L: lua_State; idx: Integer): LongBool; cdecl; external cLuaLibFileName;
  function lua_isuserdata(L: lua_State; idx: Integer): LongBool; cdecl; external cLuaLibFileName;
  function lua_type(L: lua_State; idx: Integer): Integer; cdecl; external cLuaLibFileName;
  function lua_typename(L: lua_State; tp: Integer): PAnsiChar; cdecl; external cLuaLibFileName;

  function lua_equal(L: lua_State; idx1, idx2: Integer): LongBool; cdecl; external cLuaLibFileName;
  function lua_rawequal(L: lua_State; idx1, idx2: Integer): LongBool; cdecl; external cLuaLibFileName;
  function lua_lessthan(L: lua_State; idx1, idx2: Integer): LongBool; cdecl; external cLuaLibFileName;

  function lua_tonumber(L: lua_State; idx: Integer): lua_Number; cdecl; external cLuaLibFileName;
  function lua_tointeger(L: lua_State; idx: Integer): lua_Integer; cdecl; external cLuaLibFileName;
  function lua_toboolean(L: lua_State; idx: Integer): LongBool; cdecl; external cLuaLibFileName;
  function lua_tolstring(L: lua_State; idx: Integer; var len: Cardinal): PAnsiChar; cdecl; external cLuaLibFileName;
  function lua_objlen(L: lua_State; idx: Integer): Cardinal; cdecl; external cLuaLibFileName;
  function lua_tocfunction(L: lua_State; idx: Integer): lua_CFunction; cdecl; external cLuaLibFileName;
  function lua_touserdata(L: lua_State; idx: Integer): Pointer; cdecl; external cLuaLibFileName;
  function lua_tothread(L: lua_State; idx: Integer): lua_State; cdecl; external cLuaLibFileName;
  function lua_topointer(L: lua_State; idx: Integer): Pointer; cdecl; external cLuaLibFileName;

  (*
  ** push functions (C/Pascal -> stack)
  *)
  procedure lua_pushnil(L: lua_State); cdecl; external cLuaLibFileName;
  procedure lua_pushnumber(L: lua_State; n: lua_Number); cdecl; external cLuaLibFileName;
  procedure lua_pushinteger(L: lua_State; n: lua_Integer); cdecl; external cLuaLibFileName;
  procedure lua_pushlstring(L: lua_State; s: PAnsiChar; len: Cardinal); cdecl; external cLuaLibFileName;
  procedure lua_pushstring(L: lua_State; s: PAnsiChar); cdecl; external cLuaLibFileName;
  function lua_pushvfstring(L: lua_State; fmt, argp: PAnsiChar): PAnsiChar; cdecl; external cLuaLibFileName;

  function lua_pushfstring(L: lua_State; fmt: PAnsiChar; args: array of const): PAnsiChar; cdecl; external cLuaLibFileName;
  procedure lua_pushcclosure(L: lua_State; fn: lua_CFunction; n: Integer); cdecl; external cLuaLibFileName;
  procedure lua_pushboolean(L: lua_State; b: LongBool); cdecl; external cLuaLibFileName;
  procedure lua_pushlightuserdata(L: lua_State; p: Pointer); cdecl; external cLuaLibFileName;
  function lua_pushthread(L: lua_State): Integer; cdecl; external cLuaLibFileName;

  (*
  ** get functions (Lua -> stack)
  *)
  procedure lua_gettable(L: lua_State; idx: Integer); cdecl; external cLuaLibFileName;
  procedure lua_getfield(L: lua_State; idx: Integer; k: PAnsiChar); cdecl; external cLuaLibFileName;
  procedure lua_rawget(L: lua_State; idx: Integer); cdecl; external cLuaLibFileName;
  procedure lua_rawgeti(L: lua_State; idx, n: Integer); cdecl; external cLuaLibFileName;
  procedure lua_createtable(L: lua_State; narr, nrec: Integer); cdecl; external cLuaLibFileName;
  function lua_newuserdata(L: lua_State; size: Cardinal): Pointer; cdecl; external cLuaLibFileName;
  function lua_getmetatable(L: lua_State; idx: Integer): LongBool; cdecl; external cLuaLibFileName;
  procedure lua_getfenv(L: lua_State; idx: Integer); cdecl; external cLuaLibFileName;

  (*
  ** set functions (stack -> Lua)
  *)
  procedure lua_settable(L: lua_State; idx: Integer); cdecl; external cLuaLibFileName;
  procedure lua_setfield(L: lua_State; idx: Integer; k: PAnsiChar ); cdecl; external cLuaLibFileName;
  procedure lua_rawset(L: lua_State; idx: Integer); cdecl; external cLuaLibFileName;
  procedure lua_rawseti(L: lua_State; idx, n: Integer); cdecl; external cLuaLibFileName;
  function lua_setmetatable(L: lua_State; idx: Integer): LongBool; cdecl; external cLuaLibFileName;
  function lua_setfenv(L: lua_State; idx: Integer): LongBool; cdecl; external cLuaLibFileName;

  (*
  ** `load' and `call' functions (load and run Lua code)
  *)
  procedure lua_call(L: lua_State; nargs, nresults: Integer); cdecl; external cLuaLibFileName;
  function lua_pcall(L: lua_State; nargs, nresults, errfunc: Integer): Integer; cdecl; external cLuaLibFileName;
  function lua_cpcall(L: lua_State; func: lua_CFunction; ud: Pointer): Integer; cdecl; external cLuaLibFileName;
  function lua_load(L: lua_State; reader: lua_Reader; data: Pointer; chunkname: PAnsiChar): Integer; cdecl; external cLuaLibFileName;
  function lua_dump(L: lua_State; writer: lua_Writer; data: Pointer): Integer; cdecl; external cLuaLibFileName;

  (*
  ** coroutine functions
  *)
  function lua_yield(L: lua_State; nresults: Integer): Integer; cdecl; external cLuaLibFileName;
  function lua_resume(L: lua_State; narg: Integer): Integer; cdecl; external cLuaLibFileName;
  function lua_status(L: lua_State): Integer; cdecl; external cLuaLibFileName;

  (*
  ** garbage-collection functions
  *)
  function lua_gc(L: lua_State; what, data: Integer): Integer; cdecl; external cLuaLibFileName;

  (*
  ** miscellaneous functions
  *)
  function lua_error(L: lua_State): Integer; cdecl; external cLuaLibFileName;
  function lua_next(L: lua_State; idx: Integer): Integer; cdecl; external cLuaLibFileName;
  procedure lua_concat(L: lua_State; n: Integer); cdecl; external cLuaLibFileName;

  function lua_getallocf(L: lua_State; ud: Pointer): lua_Alloc; cdecl; external cLuaLibFileName;
  procedure lua_setallocf(L: lua_State; f: lua_Alloc; ud: Pointer); cdecl; external cLuaLibFileName;

  (*
  ** {======================================================================
  ** Debug API
  ** =======================================================================
  *)
  function lua_getstack(L: lua_State; level: Integer; var ar: lua_Debug): Integer; cdecl; external cLuaLibFileName;
  function lua_getinfo(L: lua_State; what: PAnsiChar; var ar: lua_Debug): Integer; cdecl; external cLuaLibFileName;
  function lua_getlocal(L: lua_State; var ar: lua_Debug; n: Integer): PAnsiChar; cdecl; external cLuaLibFileName;
  function lua_setlocal(L: lua_State; var ar: lua_Debug; n: Integer): PAnsiChar; cdecl; external cLuaLibFileName;
  function lua_getupvalue(L: lua_State; funcindex, n: Integer): PAnsiChar; cdecl; external cLuaLibFileName;
  function lua_setupvalue(L: lua_State; funcindex, n: Integer): PAnsiChar; cdecl; external cLuaLibFileName;

  function lua_sethook(L: lua_State; func: lua_Hook; mask, count: Integer): Integer; cdecl; external cLuaLibFileName;
  function lua_gethook(L: lua_State): lua_Hook; cdecl; external cLuaLibFileName;
  function lua_gethookmask(L: lua_State): Integer; cdecl; external cLuaLibFileName;
  function lua_gethookcount(L: lua_State): Integer; cdecl; external cLuaLibFileName;

  (* lua libraries *)
  function luaopen_base(L: lua_State): Integer; cdecl; external cLuaLibFileName;
  function luaopen_debug(L: lua_State): Integer; cdecl; external cLuaLibFileName;
  function luaopen_io(L: lua_State): Integer; cdecl; external cLuaLibFileName;
  function luaopen_math(L: lua_State): Integer; cdecl; external cLuaLibFileName;
  function luaopen_os(L: lua_State): Integer; cdecl; external cLuaLibFileName;
  function luaopen_package(L: lua_State): Integer; cdecl; external cLuaLibFileName;
  function luaopen_string(L: lua_State): Integer; cdecl; external cLuaLibFileName;
  function luaopen_table(L: lua_State): Integer; cdecl; external cLuaLibFileName;
  
  (* open all previous libraries *)
  procedure luaL_openlibs(L: lua_State); cdecl; external cLuaLibFileName;

  procedure luaL_register(L: lua_State; libname: PAnsiChar; lr: PluaL_reg); cdecl; external cLuaLibFileName;
  function luaL_getmetafield(L: lua_State; obj: Integer; e: PAnsiChar): Integer; cdecl; external cLuaLibFileName;
  function luaL_callmeta(L: lua_State; obj: Integer; e: PAnsiChar): Integer; cdecl; external cLuaLibFileName;
  function luaL_typerror(L: lua_State; narg: Integer; tname: PAnsiChar): Integer; cdecl; external cLuaLibFileName;
  function luaL_argerror(L: lua_State; narg: Integer; extramsg: PAnsiChar): Integer; cdecl; external cLuaLibFileName;
  function luaL_checklstring(L: lua_State; narg: Integer; var len: Cardinal): PAnsiChar; cdecl; external cLuaLibFileName;
  function luaL_optlstring(L: lua_State; narg: Integer; d: PAnsiChar; var len: Cardinal): PAnsiChar; cdecl; external cLuaLibFileName;
  function luaL_checknumber(L: lua_State; narg: Integer): lua_Number; cdecl; external cLuaLibFileName;
  function luaL_optnumber(L: lua_State; narg: Integer; d: lua_Number): lua_Number; cdecl; external cLuaLibFileName;

  function luaL_checkinteger(L: lua_State; narg: Integer): lua_Integer; cdecl; external cLuaLibFileName;
  function luaL_optinteger(L: lua_State; narg: Integer; d: lua_Integer): lua_Integer; cdecl; external cLuaLibFileName;

  procedure luaL_checkstack(L: lua_State; sz: Integer; msg: PAnsiChar); cdecl; external cLuaLibFileName;
  procedure luaL_checktype(L: lua_State; narg, t: Integer); cdecl; external cLuaLibFileName;
  procedure luaL_checkany(L: lua_State; narg: Integer); cdecl; external cLuaLibFileName;

  function luaL_newmetatable(L: lua_State; tname: PAnsiChar): Integer; cdecl; external cLuaLibFileName;
  function luaL_checkudata(L: lua_State; narg: Integer; tname: PAnsiChar): Pointer; cdecl; external cLuaLibFileName;

  function luaL_checkoption(L: lua_State; narg: Integer; def: PAnsiChar; lst: array of PAnsiChar): Integer; cdecl; external cLuaLibFileName;

  procedure luaL_where(L: lua_State; lvl: Integer); cdecl; external cLuaLibFileName;
  function luaL_error(L: lua_State; fmt: PAnsiChar; args: array of const): Integer; cdecl; external cLuaLibFileName;

  function luaL_ref(L: lua_State; t: Integer): Integer; cdecl; external cLuaLibFileName;
  procedure luaL_unref(L: lua_State; t, ref: Integer); cdecl; external cLuaLibFileName;

{$ifdef LUA_COMPAT_GETN}
  function luaL_getn(L: lua_State; t: Integer): Integer; cdecl; external cLuaLibFileName;
  procedure luaL_setn(L: lua_State; t, n: Integer); cdecl; external cLuaLibFileName;
{$endif}

  function luaL_loadfile(L: lua_State; filename: PAnsiChar): Integer; cdecl; external cLuaLibFileName;
  function luaL_loadbuffer(L: lua_State; buff: PAnsiChar; sz: Cardinal; name: PAnsiChar): Integer; cdecl; external cLuaLibFileName;
  function luaL_loadstring(L: lua_State; s: PAnsiChar): Integer; cdecl; external cLuaLibFileName;

  function luaL_newstate(): lua_State; cdecl; external cLuaLibFileName;
  function luaL_gsub(L: lua_State; s, p, r: PAnsiChar): PAnsiChar; cdecl; external cLuaLibFileName;
  function luaL_findtable(L: lua_State; idx: Integer; fname: PAnsiChar; szhint: Integer): PAnsiChar; cdecl; external cLuaLibFileName;

  procedure luaL_buffinit(L: lua_State; var B: luaL_Buffer); cdecl; external cLuaLibFileName;
  function luaL_prepbuffer(var B: luaL_Buffer): PAnsiChar; cdecl; external cLuaLibFileName;
  procedure luaL_addlstring(var B: luaL_Buffer; s: PAnsiChar; l: Cardinal); cdecl; external cLuaLibFileName;
  procedure luaL_addstring(var B: luaL_Buffer; s: PAnsiChar); cdecl; external cLuaLibFileName;
  procedure luaL_addvalue(var B: luaL_Buffer); cdecl; external cLuaLibFileName;
  procedure luaL_pushresult(var B: luaL_Buffer); cdecl; external cLuaLibFileName;

